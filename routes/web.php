<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\ApiController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::prefix('api')->group(function () {
   
});


/*
|--------------------------------------------------------------------------
| Non Authenticate Admin Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['not_authenticated:admin']], function () {
    Route::get('/', [AdminController::class, 'showlogin'])->name("ShowAdminLogin");
});


/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
*/
Route::group(['middleware' => ['authenticated:admin']], function () {
    
    Route::get('/dashboard', [AdminController::class, 'dashboard'])->name("Dashboard");
    Route::get('/settings', [AdminController::class, 'showsettings'])->name("admin.settings");

});

Route::get('/logout',[UserController::class, 'logout'])->name("logout");