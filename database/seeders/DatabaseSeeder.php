<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Models\User;
use App\Models\Role;
use App\Models\Profile;

use function GuzzleHttp\Promise\queue;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();// Insert some stuff
        $query = Role::where("role","admin")->first();
        if(!$query){
            DB::table('roles')->insert([
                array(
                    'role' => "admin",
                ),
            ]);
        }
        
        $user = User::where("email", env('ADMIN_EMAIL'))->first();
        if(!$user){
            // Creating Admin User Credentials
            $user = User::create([
                'email' => env('ADMIN_EMAIL'),
                'password' => "Abc123",
            ]);
        }
        
        $query = Profile::where("user_id", $user->id)->first();
        if(!$query){
            // Adding Admin User Information like Name etc
            Profile::create([
                'user_id' => $user->id,
                'first_name' => "Admin",
                'last_name'=>"",
            ]);
        }

        // Getting Admin User
        $user = User::find($user->id);
        // Getting Admin Role Id
        $get_admin_role_id = Role::where('role', "admin")->first();

        // Assigning Admin Role Id to User
        $query = $user->roles()->where("role", "admin")->first();
        if(!$query){
            $user->roles()->attach($get_admin_role_id->id);
        }
    }
}