const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    onOpen: (toast) => {
        toast.addEventListener('mouseenter', Swal.stopTimer)
        toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
})

/*******************************
 *Change btn text with loader
 *******************************/
function replaceBtnWithLoader(bool, btn_text, btn, btn_type = "text") {
    if (btn.is('input')) {
        if (bool) {
            btn.val('Please wait...')
        } else {
            btn.val(btn_text)
        }

    } else {
        if (bool) {
            btn.html('<i class="fas fa-spinner fa-pulse"></i>')
        } else {
            btn.text(btn_text)
        }
    }

}

function getRequest(url, params, callBackFunction = null){
    axios.get(url, {params}).then(function (response) {
        if(callBackFunction instanceof Function){
            callBackFunction(response);
            return;
        } 
    }).catch(function (error) {
        Toast.fire({
            icon: 'error',
            title: error.response.data.error
        });
    })
}


function postRequest(formId, url, dontReloadPage = true, callBackFunction = null, customFormData = null){

    let btn = $(formId+" [type=submit]");
    let btn_text = btn && btn.is('input') ? btn.val() : btn.text()
    
    replaceBtnWithLoader(true, btn_text, btn)
    btn.prop('disabled', true);

    if(customFormData){
        var formData = customFormData;
    } else {
        var formData = new FormData($(formId)[0]);
    }  
    let config = {
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    }
    axios.post(url, formData, config).then(function (response) {
        // If you want to run your own function
        if(callBackFunction instanceof Function){
            callBackFunction(response);
            return;
        }
        Toast.fire({
            icon: 'success',
            title: response.data.message
        });
        if(dontReloadPage == true){
            
            // This will on success
            setTimeout(() => {
                location.reload();
            }, 3000);

        } else {

            // This will run if you don't want to reload the page
            // Remove property of disable from current buttons
            btn.prop('disabled', false);
            replaceBtnWithLoader(false, btn_text, btn)
            // Reset the form
            $(formId)[0].reset();
            // Hide the error div
        
        }
    }).catch(function (error) {
        btn.prop('disabled', false);
        replaceBtnWithLoader(false, btn_text, btn)
        if(error.response.status == 422){
            for(fieldError of error.response.data){
                Toast.fire({
                    icon: 'error',
                    title: fieldError.message
                });
                break;
            }
        } else {
            Toast.fire({
                icon: 'error',
                title: error.response.data.message
            });
        }
    });
}

function goBack() {
    window.history.back();
}

$(document).ready(function(){
    

    $("html").on("mousedown touchstart", "button, .ripple", function(evt) {
        var btn = $(evt.currentTarget);
        var x = evt.pageX - btn.offset().left;
        var y = evt.pageY - btn.offset().top;
        
        var duration = 800;
        var animationFrame, animationStart;
        
        var animationStep = function(timestamp) {
            if (!animationStart) {
            animationStart = timestamp;
            }
            
            var frame = timestamp - animationStart;
            if (frame < duration) {
            var easing = (frame/duration) * (2 - (frame/duration));
            
            var circle = "circle at " + x + "px " + y + "px";
            var color = "rgba(0, 0, 0, " + (0.3 * (1 - easing)) + ")";
            var stop = 90 * easing + "%";
        
            btn.css({
                "background-image": "radial-gradient(" + circle + ", " + color + " " + stop + ", transparent " + stop + ")"
            });
        
            animationFrame = window.requestAnimationFrame(animationStep);
            } else {
            $(btn).css({
                "background-image": "none"
            });
            window.cancelAnimationFrame(animationFrame);
            }
            
        };
        
        animationFrame = window.requestAnimationFrame(animationStep);
    
    });
})

 // Tag User Sound
 var audio_source = "/notifications/pristine.mp3";
 var sound_audio = new Audio();
 // no event listener needed here
 sound_audio.src = audio_source;
 // Tag User Sound Ended

function themeNotification(data) {
    let notification_markup = `
        <a href="${data.url}" class="notification-item unread">
            <span class="title">${data.title}</span>
            <span class="notification-body">
                ${data.body}
            </span>
            <h5 class="notification-date">${data.datetime}</h5>
        </a>`;
    $(".notification span").addClass("active");
    sound_audio.play();
    $(notification_markup).prependTo('#notification')
}


