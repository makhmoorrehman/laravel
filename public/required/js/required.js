window.onload = () => {
    $(".loader-spinner").fadeOut();
}
$(document).ready(function () {
    /*Menu toggle*/
    $('.navbar-toggler').on('click', function (e) {
        e.preventDefault()
        e.stopImmediatePropagation()
        $('.navbar-nav').toggleClass('menu-toggle')
    });
   
    $(".customModalBtn").on("click", function(e){
        e.preventDefault();
        e.stopImmediatePropagation();
        $(".overlay").fadeIn();
        const currentBtn = $(this).attr("data-modal");
        $(".customModal").removeClass("active");
        $(currentBtn).toggleClass("active");
    });
    
    $(".customModal .close-button").on("click", function(){
        const currentModal = $(this).attr("data-modal");
        $("#"+currentModal).toggleClass("active");
        $(".overlay").fadeOut();
    });

    $(".overlay").on("click", () => {
        $(".customModal").removeClass("active");
        $(".overlay").fadeOut();
    });
});