
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Welcome to Sambara Travels & Tours</title>
    <link href="https://fonts.googleapis.com/css?family=Karla:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.materialdesignicons.com/4.8.95/css/materialdesignicons.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{ asset('assets/css/bd-coming-soon.css') }}">
</head>

<body class="min-vh-100 d-flex flex-column">

    <header>
        <div class="container">
            <nav class="navbar navbar-dark bg-transparenet">
                <a class="navbar-brand" href="#">
                    <img src="{{ asset('assets/images/logo.png') }}" alt="logo">
                </a>
                <span class="navbar-text ml-auto d-none d-sm-inline-block">+92 324 9273684 </span>
                <span class="navbar-text d-none d-sm-inline-block">sambaratravels@gmail.com</span>
            </nav>
        </div>
    </header>
    <main class="my-auto">
        <div class="container">
            <h1 class="page-title">We're coming soon</h1>
            <p class="page-description">Welcome to Sambara Travels & Tours. We are coming to launching wonderful journies toward the richest civilization. You will be mesmerized to see these epic sites of Indus Valley. 
            </p>
            <p>Stay connected</p>
            <nav class="footer-social-links">
                <a href="https://web.facebook.com/Sambara-Travel-and-Tours-100449395846119" class="social-link"><i class="mdi mdi-facebook-box"></i></a>
                <a href="https://twitter.com/SambaraTravels" class="social-link"><i class="mdi mdi-twitter"></i></a>
				<a href="https://instagram.com/SambaraTravels" class="social-link"><i class="mdi mdi-instagram"></i></a>
                <a href="https://www.youtube.com/channel/UC9_1nwYsH_cvqYJoCkY4ccQ" class="social-link"><i class="mdi mdi-youtube"></i></a>
            </nav>
        </div>
    </main>
</body>

</html>