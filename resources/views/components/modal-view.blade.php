<div id={{ $id }} class="customModal shadow" style={{ $style }}>
    <div class="loader">
        <img src="{{asset('required/loaders/puff.svg')}}" width="100" alt="">
    </div>
    <div class="header">
        <div class="title">{{ $title }}</div>
        <button data-modal={{ $id }} class="close-button">
            <i class="fas fa-times"></i>
        </button>
    </div>
    <div class="error">{{ $error }}</div>
    <div class="body" style='{{ $body_style }}'>
        {{ $slot }}
    </div>
</div>