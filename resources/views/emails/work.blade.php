@component('mail::message')
#


Name: {{$inputs["name"]}} <br>
Email: {{$inputs["email"]}} <br>
Phone: {{$inputs["phone"]}} <br>


@component('mail::button', ['url' =>  asset('website/attachment/'.$attachement)])
View Attachment
@endcomponent

Thanks,<br>
{{ config('app.name') }}
@endcomponent