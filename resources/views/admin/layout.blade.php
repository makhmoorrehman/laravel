<!DOCTYPE html>
<html dir="ltr" lang="en">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    <!-- Favicon icon -->
    <link rel="icon" href="{{asset('website/images/favicon.png')}}" type="image/png" sizes="16x16">
    <title>Website | Admin</title>
    
    <!-- Custom CSS -->
    <link href="{{asset('admin/assets/extra-libs/c3/c3.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/libs/chartist/dist/chartist.min.css')}}" rel="stylesheet">
    <link href="{{asset('admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.css')}}" rel="stylesheet" />
    
    <!-- My Own Custom Css -->
    <link rel="stylesheet" type="text/css" href="{{asset('required/css/required.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('required/css/imp.css')}}">

    <!-- FontAwesome -->
    <script src="https://kit.fontawesome.com/52a93f3999.js" crossorigin="anonymous"></script>

    <!-- Default Theme (Core) -->
    <link rel="stylesheet" href="{{asset('required/datetimepicker/themes/default.css')}}">
    <!-- Default Theme (Date Picker) -->
    <link rel="stylesheet" href="{{asset('required/datetimepicker/themes/default.date.css')}}">
    <!-- Default Theme (Time Picker If Needed)-->
    <link rel="stylesheet" href="{{asset('required/datetimepicker/themes/default.time.css')}}">
    
    <!-- Custom CSS -->
    <link href="{{asset('admin/dist/css/style.css')}}" rel="stylesheet">
    @yield('style')
    
    <script src="https://cdn.ckeditor.com/ckeditor5/33.0.0/classic/ckeditor.js"></script>
</head>

<body>
    <div class="overlay"></div>
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="lds-ripple">
            <div class="lds-pos"></div>
            <div class="lds-pos"></div>
        </div>
    </div>


    <div id="main-wrapper" data-theme="light" data-layout="vertical" data-navbarbg="skin6" data-sidebartype="full"
        data-sidebar-position="fixed" data-header-position="fixed" data-boxed-layout="full">
        
        @include('admin.partials.Menu')

        <div class="page-wrapper">
            @yield('content')

            <footer class="footer text-center text-muted">
                &copy; Copyright 2022 - <a target="_blank" href="https://makhmoor.com">website</a> Developed by <a target="_blank" href="https://makhmoor.com">Makhmoor</a>
            </footer>
        </div>
    
    </div>

    <script src="{{asset('admin/assets/libs/jquery/dist/jquery.min.js')}}"></script>
    <script src="{{asset('admin/assets/libs/popper.js/dist/umd/popper.min.js')}}"></script>
    <script src="{{asset('admin/assets/libs/bootstrap/dist/js/bootstrap.min.js')}}"></script>
    
    <!-- apps -->
    <!-- apps -->
    <script src="{{asset('admin/dist/js/app-style-switcher.js')}}"></script>
    <script src="{{asset('admin/dist/js/feather.min.js')}}"></script>

    <script src="{{asset('admin/assets/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js')}}"></script>
    <script src="{{asset('admin/dist/js/sidebarmenu.js')}}"></script>
    
    <!--Custom JavaScript -->
    <script src="{{asset('admin/dist/js/custom.min.js')}}"></script>
    
    
    <!--This page JavaScript -->
    <script src="{{asset('admin/assets/extra-libs/c3/d3.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/c3/c3.min.js')}}"></script>
    <script src="{{asset('admin/assets/extra-libs/jvector/jquery-jvectormap-2.0.2.min.js')}}"></script>

    <script src="{{asset('admin/assets/extra-libs/jvector/jquery-jvectormap-world-mill-en.js')}}"></script>
    <script src="{{asset('admin/dist/js/pages/dashboards/dashboard1.min.js')}}"></script>

    <script src="{{asset('required/js/sweetalert.min.js')}}"></script>


    <!-- Axios -->
    <script src="{{asset('required/js/axios.min.js')}}"></script>
        
    <!-- Jquery -->
    <script src="{{asset('required/js/custom.js')}}"></script>
    <script src="{{asset('required/js/required.js')}}"></script>
    
    <script src="{{asset('required/datetimepicker/picker.js')}}"></script>
    <script src="{{asset('required/datetimepicker/picker.date.js')}}"></script>
    <script src="{{asset('required/datetimepicker/picker.time.js')}}"></script>
    <script src="{{asset('required/datetimepicker/legacy.js')}}"></script>
    <script>
        // date picker
        $('.datepicker').pickadate({
            format:'yyyy-mm-dd',
        });

        // time picker
        $('.timepicker').pickatime({
            format:'HH:i'
        });
    </script>
    @yield('script')
</body>
</html>