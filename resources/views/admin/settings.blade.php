@extends('admin.layout')

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12 align-self-center">
            <h3 class="page-title text-truncate text-dark font-weight-medium mb-1">Manage Settings</h3>
            <div class="d-flex align-items-center">
                <nav aria-label="breadcrumb">
                    <ol class="breadcrumb m-0 p-0">
                        <li class="breadcrumb-item">
                            <a href="{{route('Dashboard')}}">Home</a>
                        </li>
                        <li class="breadcrumb-item text-muted active" aria-current="page">Settings</li>
                    </ol>
                </nav>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-6">
            <div class="card">
                <div class="card-body">
                    <form id="updatePasswordForm" method="POST">
                        @csrf
                        <div class="form-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <h4 class="card-title md-40">Update Password</h4>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Old Password</label>
                                        <input type="password" class="form-control" required name="old_password" placeholder="Old Password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>New Password*</label>
                                        <input type="password" class="form-control" required name="password" placeholder="New Password">
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label>Confirm New Password*</label>
                                        <input type="password" class="form-control" required name="confirm_new_password" placeholder="Confirm New Password">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="text-right">
                                <button type="submit" class="btn btn-info">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    $("form#updatePasswordForm").on("submit", function(e){
        e.preventDefault();          
        postRequest("form#updatePasswordForm", "{{route('UpdateAdminPassword')}}");
    });
</script>
@endsection