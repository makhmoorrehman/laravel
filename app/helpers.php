<?php

if(!function_exists("addMinutesToTime")){
    function addMinutesToTime($datetime,$minutesToAdd,$format)
    {
        $output = new DateTime($datetime);
        $output->modify("+{$minutesToAdd} minutes");
        return $output->format($format);
    }
}

if(!function_exists("dateFormat")){
    function dateFormat($datetime,$format)
    {
        $output = new DateTime($datetime);
        return $output->format($format);
    }
}

if(!function_exists("convertTextAmountToNumber")){
    function convertTextAmountToNumber($amount){
        $amt = floatval($amount) / 100;
        return $amt;
    }
}

if(!function_exists("capitalizeWords")){
    function capitalizeWords($text){
        return ucwords($text);
    }
}