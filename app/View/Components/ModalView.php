<?php

namespace App\View\Components;

use Illuminate\View\Component;

class ModalView extends Component
{
    /**
     * Create a new component instance.
     *
     * @return void
     */
    public $id;
    public $title;
    public $style;
    public $body_style;
    public $error;

    public function __construct($id, $title, $style = null, $body_style = null, $error = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->style = $style;
        $this->body_style = $body_style;
        $this->error = $error;
    }

    /**
     * Get the view / contents that represent the component.
     *
     * @return \Illuminate\Contracts\View\View|string
     */
    public function render()
    {
        return view('components.modal-view');
    }
}
