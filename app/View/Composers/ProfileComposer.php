<?php
namespace App\View\Composers;


use Illuminate\View\View;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

use App\Models\User;
use App\Models\Role;
use App\Models\UserType;


class ProfileComposer
{
    /**
     * Bind data to the view.
     *
     * @param  \Illuminate\View\View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user = Auth::user();
        if($user){
            $user = User::find($user->id);
            $view->with("user", $user);
        }
    }
}