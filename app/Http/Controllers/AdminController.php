<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

use DateTime;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;
use Stripe;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\Builder;

class AdminController extends Controller
{
    public $user;

    function __construct(User $user) {
        $this->user = $user;
    }

    /* ====================== VIEWS START ====================== */
    
    public function dashboard() { // ----- > Dashboard Page 
        $data = [];
        return view('admin/dashboard', $data);
    }
    
    public function showlogin() { // ----- > Login Page
        return view('admin/login');
    } 

    public function showsettings(){ // ----- > Profile Settings
        return view('admin/settings');
    }
    /* ====================== VIEW END ====================== */


    

    /* ======================== POST FUNCTIONS  ======================= */
    /* ====================== POST FUNCTIONS END ====================== */
}