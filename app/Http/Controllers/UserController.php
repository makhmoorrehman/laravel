<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\User;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public $user;

    function __construct(User $user) {
        $this->user = $user;
    }

    public function login(Request $request)
    {
        $credentials = $request->only('email', 'password');
        if(Auth::attempt($credentials)){
            $request->session()->regenerate();
            return response()->json(['message' => "Access Granted!"]);
        }
        return $this->senderror("Access Denied!", 404);
    }
    
    public function updatepassword(Request $request)
    {
        $query = null;
        if($request->confirm_new_password == $request->password){
            $query = User::where("id",$request->user()->id)->first();
            if (Hash::check($request->old_password, $query->password)) {
                $query = User::where('id', $request->user()->id)->update(['password' =>  Hash::make($request->password)]);
                if($query){
                    return response()->json(['message' => "Password Updated!"]);
                }
            } else {
                return $this->senderror("Invalid Password", 404);
            }
        } else {
            return $this->senderror("Confirm password not match!", 404);
        }
    }
    
    public function logout(Request $request)
    {
        Auth::logout();
        $request->session()->invalidate();
        $request->session()->regenerateToken();
        return redirect('/');
    }
}