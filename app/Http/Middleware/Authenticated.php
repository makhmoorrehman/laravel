<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class Authenticated
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        if (!$request->user()) {
            if($role == "admin"){
                return redirect(route("ShowAdminLogin"));
            }
            return redirect(route("/"));
        }

        // Check if route is for admin or not
        if($role == "admin"){
            
            // Check if user is admin or not
            $query = User::find($request->user()->id)->roles->where("role","admin")->first();
            if(!$query){
                return redirect('/404');
            }

        }

        return $next($request);
    }

}