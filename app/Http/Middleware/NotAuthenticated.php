<?php

namespace App\Http\Middleware;

use Closure;
use App\Models\User;

class NotAuthenticated
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string  $role
     * @return mixed
     */
    public function handle($request, Closure $next, $role = null)
    {
        if ($request->user()) {
            return redirect(route("Dashboard"));
        }
        return $next($request);
    }

}